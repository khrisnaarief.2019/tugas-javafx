
package org.openjfx.controller;

import eu.hansolo.medusa.Gauge;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;

public class Control {
    private int number = 0;

    @FXML
    private Gauge gauge1;

    @FXML
    private Label text1;

    @FXML
    void onButtonAddClick(ActionEvent event) {
        if (number < 200) {
            number += 1;
            text1.setText(Integer.toString(number));
            gauge1.setValue(number / 2);
        } else if (number >= 200) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Peringatan");
            alert.setHeaderText("AIR PENUH");
            alert.setContentText("Air sudah penuh. Tidak dapat menambah air lagi");
            alert.showAndWait();
        }
    }

    @FXML
    void onButtonDecClick(ActionEvent event) {
        number -= 1;
        text1.setText(Integer.toString(number));
        gauge1.setValue(number / 2);
    }

}
